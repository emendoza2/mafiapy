import settings
from os import getenv
BOT_TOKEN = getenv('BOT_TOKEN')

import discord
client = discord.Client()

import random

@client.event 
async def on_ready():
    print('Logged on as {0.user}!'.format(client))

@client.event
async def on_message(message):
        if message.author == client.user:
            return
        if message.content.startswith('$hello'):
            await message.channel.send('hello!')
            print('Message from {0.author}: {0.content}'.format(message))
        if message.content.startswith('$ping'):
            await message.channel.send(client.latency)
        if message.content.startswith('$rand'):
            await message.channel.send(random.randint(0,100))

client.run(BOT_TOKEN)